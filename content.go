package main

import (
	"embed"
	"io/fs"
)

//go:embed static
var content embed.FS
var static, _ = fs.Sub(content, "static")

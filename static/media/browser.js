if (document.readyState === "complete") {
	main();
} else {
	document.addEventListener("DOMContentLoaded", ()=>main());
}

function main() {
	if (location.hash.length < 2) {
		setLocation("/");
		history.pushState(null, '', '#/');
		return
	}

	setLocation(location.hash.slice(1));
}

var currentLocation = "/";

window.addEventListener("popstate", ()=>setLocation(location.hash.slice(1)));

function setLocation(location) {
	fetch("../api/media" + location)
		.then(response => response.json())
		.then(data => {
			if (data.error) {
				console.error("server says:", data.error);
				alert("server returned error: " + data.error);
				return;
			}
			document.title = location + " - Webcon media browser";
			currentLocation = location;
			populateList(data.files);
		})
		.catch(e => console.error(e));
}

function populateList(files) {
	let rows = [];

	for (let file of files) {
		let row = document.createElement("tr");
		row.dataset['filename'] = normalizedName(file.name);
		row.append(
			document.createElement("td"),
			document.createElement("td"),
			document.createElement("td"),
		);

		let a = document.createElement("a");
		a.innerText = file.name;
		a.title = file.name;
		a.href = "../api/media" + currentLocation + file.name;
		if (file.name.match(/\/$/)) {
			a.addEventListener("click", clickEv => {
				clickEv.preventDefault();
				setLocation(currentLocation + file.name);
				history.pushState(null, '', '#' + currentLocation + file.name);
			});
		}
		row.children[0].append(a);

		row.children[1].innerText = new Date(file.modified).toDateString();
		row.children[2].innerText = file.size;

		rows.push(row);
	}

	rows.sort((a,b) => a.dataset['filename'] > b.dataset['filename']);

	if (currentLocation.length > 1) {
		rows.unshift(document.createElement("tr"));
		rows[0].append(document.createElement("td"));
		let uplink = document.createElement("a");
		uplink.innerText = '../'
		uplinkLocation = currentLocation.replace(/\/[^\/]*\/?$/, "/");
		uplink.href = location.pathname + '#' + uplinkLocation;
		uplink.addEventListener("click", clickEv => {
			clickEv.preventDefault();
			let up = uplinkLocation
			setLocation(up);
			history.pushState(null, '', '#' + up);
		});
		rows[0].children[0].append(uplink);
	}

	let tbody = document.getElementById("dir-entries");
	tbody.innerHTML = "";
	tbody.append(...rows);

	document.getElementById("current-path").innerText = decodeURIComponent(currentLocation);
}

function normalizedName(name) {
	if (!name) { return ''; }
	name = name.toLowerCase();
        const tagRemoverRegex = /^(\[.+?\][ _]*)*/g;
        name = name.replace(/\./g, ' ');
        return name.replace(tagRemoverRegex, '') || '';
}

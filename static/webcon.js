const playingName = document.getElementById("playingname");

const playPauseBtn = document.getElementById("playpause");
const playPauseImg = playPauseBtn.children[0];
const playImgUrl = "icons/play.svg";
const pauseImgUrl = "icons/pause.svg";

const loopFileBtn = document.getElementById("loopfile");
const loopFileImg = loopFileBtn.children[0];
const imageLoopingUrl = "icons/looping-current.svg";
const imageNotLoopingUrl = "icons/loop.svg";

const seekerBar = document.getElementById("seeker");
const seekerLabel = seekerBar.labels[0];
const seekerTooltipWrapper = document.getElementById("seeker-tooltip-wrapper");
const seekerTooltip = document.getElementById("seeker-tooltip");

const queueList = document.getElementById("queuelist");

const timeProgress = document.getElementById("time-progress");
const timeTotal = document.getElementById("time-total");
const timeRemaining = document.getElementById("time-remaining");

const clockHm = document.getElementById("clock-hm");
const clockS = document.getElementById("clock-s");

const nothingPlayingText = "Nothing is playing.";

window.addEventListener('load', ev => {
	let interaction = document.getElementById("interaction");
	let controls = document.getElementById("controls");
	interaction.style.paddingBottom = controls.clientHeight + "px";
});

document.addEventListener('DOMContentLoaded', (ev) => {
	playPauseBtn.addEventListener('click', ()=>controlCall("toggle"));
	loopFileBtn.addEventListener('click',toggleLoop);

	document.getElementById("stop").addEventListener('click', ()=>controlCall("stop"));
	document.getElementById("prev").addEventListener('click', ()=>controlCall("previous"));
	document.getElementById("next").addEventListener('click', ()=>controlCall("next"));
	document.getElementById("seekback").addEventListener('click',
		()=>controlCall("seek", ["type", "relative", "time", -10]));
	document.getElementById("seekforward").addEventListener('click',
		()=>controlCall("seek", ["type", "relative", "time", 10]));

	// change is called on a 'range' input when the user lets go of the slider
	seekerBar.addEventListener('change',
		(ev)=>controlCall("seek", ["type", "absolute", "time", ev.target.value]));
	// input is called on a 'range' input when the user moves the slider
	seekerBar.addEventListener('input', ()=>{});

	document.getElementById("loadform").addEventListener('submit', loadMedia);
	document.getElementById("subloadform").addEventListener('submit', addSubtitle);

	seekerBar.addEventListener("mouseenter", e => seekerTooltip.style.display = "block");
	seekerBar.addEventListener("mouseleave", e => seekerTooltip.style.display = "none");
	seekerBar.addEventListener("mousemove", e => {
		seekerTooltipWrapper.style.left = e.clientX + "px";
		let pos = parseInt(seekerBar.max) * (e.offsetX / seekerBar.offsetWidth);
		if (pos > seekerBar.max) pos = parseInt(seekerBar.max);
		if (pos < seekerBar.min) pos = parseInt(seekerBar.min);
		seekerTooltip.innerText = timeStamp(pos);
	});

	setInterval(()=>{
		const leftPadClock = n => n.toString().replace(/^([\d])$/, "0$1");
		let d = new Date();
		let clockString = [
			d.getHours(),
			d.getMinutes(),
		].map(leftPadClock).join(':');
		clockHm.innerText = clockString;
		clockS.innerText = leftPadClock(d.getSeconds());
	}, 200);

	updatePlayerState();
	setInterval(updatePlayerState, 600);
	populateMedia(document.getElementById("media-list"));
});

document.addEventListener("keydown", ev => {
	if (ev.isComposing || ev.keyCode == 229) { // block the composition events
		return;
	}

	if (ev.target.tagName == "INPUT") return; // i wanna type spaces

	if (ev.keyCode == 32) {
		console.log(ev);
		ev.preventDefault();
		controlCall("toggle");
	}
});

var updateInProgress = 0;

function updatePlayerState() {
	if (updateInProgress !== 0) {
		return
	}

	updateInProgress = 1;
	fetch("/api/player/state")
		.then(resp => resp.json())
		.then(data => {
			if (data.title.length == 0) {
				updateWithDiff(playingName, nothingPlayingText, "innerText");
				updateWithDiff(playingName, nothingPlayingText, "title");
				playingName.innerText = nothingPlayingText;
				playingName.title = nothingPlayingText;
			} else {
				updateWithDiff(playingName, data.title, "innerText");
				updateWithDiff(playingName, data.title, "title");
			}

			if (data.paused && playPauseImg.src.endsWith(pauseImgUrl))
				playPauseImg.src = playImgUrl;
			else if (!data.paused && playPauseImg.src.endsWith(playImgUrl))
				playPauseImg.src = pauseImgUrl;

			if (data["loop-file"] && !loopFileBtn.classList.contains("active")) {
				loopFileBtn.classList.add("active");
				loopFileImg.src = imageLoopingUrl;
			} else if (!data["loop-file"] && loopFileBtn.classList.contains("active")) {
				loopFileBtn.classList.remove("active");
				loopFileImg.src = imageNotLoopingUrl;
			}

			let totalLength = Math.ceil(data.elapsed + data.remaining);
			let timePos = Math.floor(data.elapsed);
			updateWithDiff(seekerBar, totalLength, "max");
			updateWithDiff(seekerBar, timePos, "value");

			if (updateWithDiff(timeProgress.dataset, timePos, "value")) {
				timeProgress.innerText = timeStamp(timePos);
			}
			if (updateWithDiff(timeTotal.dataset, totalLength, "value")) {
				timeTotal.innerText = timeStamp(totalLength);
			}
			if (updateWithDiff(timeRemaining.dataset, totalLength - timePos, "value")) {
				timeRemaining.innerText = "-" + timeStamp(totalLength - timePos);
			}

			if (data.playlist) for (let item of data.playlist) {
				// TODO: don't update items which haven't changed
				let htmlString = "";
				for (let item of data.playlist) {
					htmlString += `<li data_id=${item.id}>${item.filename}</li>\n`;
				}
				queueList.innerHTML = htmlString;
			}

			updateInProgress = 0;
		})
		.catch(err => {
			console.error(`error during status update ${Date()}:`, err);
			updateInProgress = 0;
		});
}

function leftPad(value, width, padWith) { // i've seen this function end empires
	value = value.toString();
	while (value.length < width) {
		value = `${padWith}${value}`;
	}
	return value;
}

function timeStamp(seconds) {
	minutes = seconds / 60;
	hours = minutes / 60 | 0;
	minutes = minutes % 60 | 0;
	seconds = seconds % 60 | 0;
	return `${leftPad(hours,2,'0')}:${leftPad(minutes,2,'0')}:${leftPad(seconds,2,'0')}`;
}

function updateWithDiff(item, value, property) {
	// returns true if object updated, false if no update necessary
	if (!item[property]) item[property] = item[property];
	if (item[property] == value) return false;
	item[property] = value;
	return true;
}

function toggleLoop() {
	if (!loopFileBtn.classList.contains("active")) {
		controlCall("loopfile", ["count", "inf"]);
	} else {
		controlCall("loopfile", ["count", "no"]);
	}
}

function loadMedia(subEv) {
	subEv.preventDefault();
	let url = subEv.target.elements["url"].value;
	let cmd = "playnow"
	subEv.target.elements["url"].value = "";

	if (subEv.submitter.id == "queuebtn") cmd = "queue";

	return controlCall(cmd, ["url", url]);
}

function addSubtitle(subEv) {
	subEv.preventDefault();
	let url = subEv.target.elements["url"].value;
	let cmd = "sub-add"
	subEv.target.elements["url"].value = "";

	return controlCall(cmd, ["url", url]);
}

function controlCall(cmd, args) {
	formData = new FormData();
	if (args) 
		for (let i = 0; i <= args.length; i += 2)
			formData.set(args[i], args[i+1]);

	fetch(`/api/player/${cmd}`, {method: "POST", body: formData})
		.then((resp)=>{
			console.log(
				resp.ok?`command '${cmd}' succeeded`:`command '${cmd}' failed`
			)
			updatePlayerState();
		})
		.catch((err)=>console.log(
			`network error during command '${cmd}': `, err
		));
}

function populateMedia(mediaList) {
	fetch("/api/media/") // TODO: location argument
		.then((resp)=>resp.json())
		//.then((json)=>alert("got media:\n" + json))
		.catch((err)=>console.log("media fetch error:\n", err));
}

// vim: ts=4:sw=4:sts

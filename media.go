package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"
)

type MediaAPIHandler struct {
	BasePath string
}

func NewMediaAPIHandler(baseUrl string) http.Handler {
	baseUrl = strings.TrimSuffix(baseUrl, "/")
	return http.StripPrefix(baseUrl, MediaAPIHandler{
		MediaBasePath,
	})
}

type TinyFile struct {
	Name     string    `json:"name"`
	Modified time.Time `json:"modified"`
	Size     int64     `json:"size"`
}

func (h MediaAPIHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	requestPath := path.Clean(r.URL.Path)
	requestPath = filepath.FromSlash(requestPath)
	requestPath = filepath.Join(h.BasePath, requestPath)
	// TODO: make following symlinks configurable
	// TODO: allow symlinks within MediaBasePath, but default-deny ones that leave it

	log.Println("user wants:", requestPath)

	fileinfo, err := os.Stat(requestPath)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(500)
		w.Write([]byte(`{"error": "could not Stat() requested path"}`))
		return
	}

	if !fileinfo.IsDir() {
		http.ServeFile(w, r, requestPath)
		return
	}

	entries, err := os.ReadDir(requestPath)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(500)
		w.Write([]byte(`{"error": "could not ReadDir()"}`))
		return
	}

	if _, exists := r.Header["Icy-Metadata"]; exists {
		/// XXX: there may be a better mime type for m3u
		w.Header().Set("Content-Type", "application/x-mpegurl")
		w.WriteHeader(200)
		for _, entry := range entries {
			w.Write([]byte(entry.Name()))
			w.Write([]byte("\r\n"))
		}
		return
	}

	files := make([]TinyFile, len(entries))
	for i, entry := range entries {
		info, err := entry.Info()
		if err != nil {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(500)
			w.Write([]byte(`{"error": "could not get Info() for dir entry"}`))
			log.Println("aaah", err)
			return
		}
		name := entry.Name()
		if entry.IsDir() {
			name = name + "/"
		}
		modtime := info.ModTime()
		size := info.Size()

		files[i] = TinyFile{
			name,
			modtime,
			size,
		}
	}

	encoded, err := json.Marshal(map[string][]TinyFile{
		"files": files,
	})
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(500)
		encoded, err = json.Marshal(map[string]string{
			"error": fmt.Sprintf("%v", err),
		})
		if err != nil {
			w.Write([]byte("media: error while formatting error"))
			return
		}
		w.Write(encoded)
		return
	} else {
		w.WriteHeader(200)
	}

	w.Write(encoded)
}

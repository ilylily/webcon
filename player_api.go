package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"
)

// PlayerAPIHandler implements http.Handler for net/http integration
type PlayerAPIHandler struct {
	ipc         *IPCHandler
	playerState *PlayerState
}

type PlayerState struct {
	Paused    bool           `json:"paused"`
	Elapsed   float64        `json:"elapsed"`
	Remaining float64        `json:"remaining"`
	Title     string         `json:"title"`
	LoopFile  bool           `json:"loop-file"`
	Playlist  []PlaylistItem `json:"playlist"`
}

type PlaylistItem struct {
	Filename string `json:"filename"`
	Current  bool   `json:"current,omitempty"`
	Playing  bool   `json:"playing,omitempty"`
	ID       int    `json:"id"`
}

// NewPlayerAPIHandler returns an PlayerAPIHandler with an open control socket to mpv
func NewPlayerAPIHandler() (*PlayerAPIHandler, error) {
	ipc, err := NewIPCHandler()
	if err != nil {
		return nil, err
	}

	ps := PlayerState{Paused: true}

	handler := PlayerAPIHandler{
		ipc:         ipc,
		playerState: &ps,
	}

	properties := make(chan IPCEvent)
	ipc.Listeners.Register("property-change", properties)
	go ps.stateObserver(ipc, properties)

	return &handler, nil
}

func (s *PlayerState) stateObserver(ipc *IPCHandler, events chan IPCEvent) {
	// add the properties in the background so we can catch their first outputs
	go func() {
		propertyNames := []string{
			"time-pos",
			"time-remaining",
			"media-title",
			"loop-file",
			"playlist",
			"pause",
		}
		for k, v := range propertyNames {
			k += 1
			response, err := ipc.Call("observe_property", k, v)
			if err != nil {
				log.Printf("stateObserver: failed to observe %s! bailing out!!", v)
				return
			}
			log.Printf("observe_property %d %s; status: %s\n", k, v, response.Error)
		}
	}()

	for event := range events {
		switch evDataType := event.Data.(type) {
		case float64:
			data := event.Data.(float64)
			switch event.Name {
			case "time-pos":
				s.Elapsed = data
			case "time-remaining":
				s.Remaining = data
			default:
				log.Printf("stateObserver: unknown float64-type event %f\n", data)
			}

		case string:
			data := event.Data.(string)
			switch event.Name {
			case "media-title":
				s.Title = data
			case "loop-file":
				s.LoopFile = true
			default:
				log.Printf("stateObserver: unknown string-type event %s\n", data)
			}

		case bool:
			data := event.Data.(bool)
			switch event.Name {
			case "loop-file":
				s.LoopFile = data
			case "pause":
				s.Paused = data

			default:
				log.Printf("stateObserver: unknown bool-type event %t\n", data)
			}

		case []interface{}:
			data := event.Data.([]interface{})
			switch event.Name {
			case "playlist":
				playlist := make([]PlaylistItem, len(data))
				for k, v := range data {
					apiItem := v.(map[string]interface{})
					playlistItem := PlaylistItem{
						Filename: apiItem["filename"].(string),
						Current:  false,
						Playing:  false,
						ID:       int(apiItem["id"].(float64)),
					}

					// these items are missing if mpv doesn't set them 'false'
					if current, ok := apiItem["current"].(bool); ok {
						playlistItem.Current = current
					}
					if playing, ok := apiItem["current"].(bool); ok {
						playlistItem.Playing = playing
					}

					playlist[k] = playlistItem
				}
				s.Playlist = playlist
			default:
			}

		case nil:
			switch event.Name {
			case "media-title":
				s.Title = ""
			default:
				log.Printf("stateObserver: can't handle nil property '%s'", event.Name)
			}
		default:
			log.Printf(
				"stateObserver: unknown event.Data "+
					"name %s "+
					"type %T "+
					"value:\n%#v\n",
				event.Name,
				evDataType,
				event.Data,
			)
		}
	}

	log.Println("stateObserver: events channel closed!")
}

func (h PlayerAPIHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	log.Printf("%s - %s %s", req.RemoteAddr, req.Method, req.URL.Path)

	routingPath := req.URL.Path[len("/api/player/"):len(req.URL.Path)]
	if len(routingPath) == 0 {
		return
	}

	pathParts := strings.SplitN(routingPath, "/", 2)
	if len(pathParts) == 0 {
		return
	}

	command := pathParts[0]

	var status int = 200
	var response *IPCResponse
	var ipcErr error

	switch req.Method {
	case "POST":
		switch command {
		case "toggle":
			response, ipcErr = h.ipc.Call("cycle", "pause")
		case "pause":
			response, ipcErr = h.ipc.Call("set_property", "pause", true)
		case "play":
			response, ipcErr = h.ipc.Call("set_property", "pause", false)
		case "loopfile":
			count := req.FormValue("count")
			response, ipcErr = h.ipc.Call("set_property", "loop-file", count)
		case "next":
			response, ipcErr = h.ipc.Call("playlist-next")
		case "previous":
			response, ipcErr = h.ipc.Call("playlist-prev")
		case "stop":
			response, ipcErr = h.ipc.Call("stop")
		case "sub-add":
			url := req.FormValue("url")
			response, ipcErr = h.ipc.Call("sub-add", url, "select")
		case "playnow":
			url := req.FormValue("url")
			response, ipcErr = h.ipc.Call("loadfile", url, "replace")
		case "queue":
			url := req.FormValue("url")
			response, ipcErr = h.ipc.Call("loadfile", url, "append-play")
		case "seek":
			seekType := req.FormValue("type")
			switch seekType {
			// only allow 'relative' and 'absolute'
			case "relative":
			case "absolute":
			default:
				seekType = "relative"
			}
			seekTime := req.FormValue("time")
			response, ipcErr = h.ipc.Call("seek", seekTime, seekType)
		default:
			ipcErr = InvalidCommandError{}
			status = http.StatusNotFound
		}

	case "GET":
		switch command {
		case "volume":
			response, ipcErr = h.ipc.Call("get_property", "ao_volume")
		case "playlist":
			response, ipcErr = h.ipc.Call("get_property", "playlist")
		case "loopfile":
			response, ipcErr = h.ipc.Call("get_property", "loop-file")
		case "state":
			respBytes, err := json.Marshal(h.playerState)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
			} else {
				w.WriteHeader(http.StatusOK)
				w.Write(respBytes)
			}
			return
		default:
			ipcErr = InvalidCommandError{}
			status = http.StatusNotFound
		}

	default:
		ipcErr = InvalidMethodError{}
		status = http.StatusBadRequest
	}

	if ipcErr != nil {
		w.WriteHeader(status)
		// TODO: better error output - JSON?
		w.Write([]byte(ipcErr.Error()))
		return
	}

	respBytes, err := json.Marshal(response)
	if err != nil {
		w.WriteHeader(404)
	} else {
		w.WriteHeader(status)
		w.Write(respBytes)
	}
}

type InvalidCommandError struct{}

func (err InvalidCommandError) Error() string { return "No such command\n" }

type InvalidMethodError struct{}

func (err InvalidMethodError) Error() string { return "Invalid method\n" }

package main

import "os"

// TODO: read this from config
const configPlayerSocketPath string = "/tmp/webcon-mpv.sock"

// CheckPlayerRunning returns an error if no player control socket is open
func CheckPlayerRunning() error {
	_, err := os.Stat(configPlayerSocketPath)
	return err
}

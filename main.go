// webcon is a web console for media playback with mpv
// api docs coming soon(tm)
package main

import (
	"log"
	"net/http"
)

// TODO: configure this with a cli argument
var MediaBasePath string = "."

func main() {
	playerApiHandler, err := NewPlayerAPIHandler()
	if err != nil {
		log.Fatal("could not create connection to mpv!")
	}

	http.Handle("/", http.FileServer(http.FS(static)))
	http.Handle("/api/player/", playerApiHandler)
	http.Handle("/api/media/", NewMediaAPIHandler("/api/media/"))

	log.Println("Serving...")
	// TODO: read config for host & port number. for now i use a reverse proxy
	log.Fatal(http.ListenAndServe("127.0.0.1:44203", nil))
}

# webcon

wow! webcon is the product of me being just. so fed up with kodi. wow

## features

webcon does things!
* rest api so that frontends can be separate from the backend
* built-in frontend so that doesn't really matter
* play urls from *The Internet* with mpv's built-in `youtube-dl` support
* and, uh, more! there's more

## requirements

webcon depends on `mpv` and optionally `youtube-dl`

grab mpv from your distro's package manager or https://mpv.io

and if you don't run a rolling-release OS like Arch Linux, Debian unstable, or OpenBSD -current, it's preferable to install `youtube-dl` from pip:
```sh
pip install --user --upgrade "youtube-dl"
```

make sure `$HOME/.local/bin` is in your `PATH` to ensure that mpv can access the latest version of youtube-dl

currently, youtube-dl requires a youtube account to play videos rated for adults. see the youtube-dl readme: https://github.com/ytdl-org/youtube-dl/blob/master/README.md

## using

webcon will crash if it can't connect to the mpv ipc socket... run mpv like this before starting webcon:

```sh
mpv --idle --input-ipc-server="/tmp/webcon-mpv.sock"
```

i also add `-fs` and `--audio-channels=2`

then just `go run .` or `go build . && ./webcon`!

## todo

planned features for the near future:

- [ ] websocket based update pipeline, replacing the current short poll
- [ ] playlist management
- [ ] plugin-based media browser
- [ ] player process management so you don't have to run mpv yourself
- [ ] clean up css
- [ ] audio/video/subtitle track selection

## Screenshots
![Webcon-Screenshot](/webcon-screenshot.png)
